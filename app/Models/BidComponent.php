<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Staudenmeir\LaravelAdjacencyList\Eloquent\HasRecursiveRelationships;

class BidComponent extends Model
{
    use HasFactory;
    use HasRecursiveRelationships;

    public function getParentKeyName()
    {
        return 'parent';
    }
    public function getLocalKeyName()
    {
        return 'ref_id';
    }

    function childLineItems() {
        return $this->hasMany(BidLineItem::class, 'parent', 'ref_id');
    }
    function childComponents() {
        return $this->hasMany(BidComponent::class, 'parent', 'ref_id')->with('childComponents');
    }
}
