<?php

namespace App\Http\Controllers;

use App\Models\Bid;
use App\Models\BidComponent;
use Illuminate\Http\Request;

class BidsController extends Controller
{
    public function index() {
        $bids = Bid::where('parent', '=', null)->get();
        return view('bidIndex')->with('bids', $bids);
    }

    public function show($id) {
        $bid = BidComponent::where('ref_id', $id)->where('parent', null)->first();
        $tree = $bid->descendants->toTree();
        $totalCost = $bid->descendants->where('entity_type', '=', 'L')->sum('cost');
        return view('bidDetail')
            ->with('bid', $bid)
            ->with('items', $tree)
            ->with('totalCost', $totalCost);
    }

    public function propagate($id) {
        $bid = BidComponent::where('ref_id', $id)->where('parent', null)->first();

    $providedValues = collect(json_decode(request('providedValues')))->mapWithKeys(function($item) {
        return [$item->component_ref_id => $item->value];
    });

    function getPropagatedValue($parentActuals, $acc) {
        $parentActual = array_pop($parentActuals);
        if($parentActual) {
            $multiplyer = $parentActual['weight'] ?? $parentActual['provided'];
            $acc = $acc * $multiplyer;
            if($parentActual['provided']) {
                return $acc;
            }
            return getPropagatedValue($parentActuals, $acc);
        }
        return $acc;
    }


    function mapToTree($parentBid, $parentActuals, $providedValues, $path = null) {
        return $parentBid->children->mapWithKeys(function($bidItem) use ($parentActuals, $path, $providedValues, $parentBid) {
            // generate path
            $path = $path ? $path . '.' . $bidItem->ref_id: $bidItem->ref_id;
            // default actual values here
            $actual = [
                'provided' => $providedValues->has($bidItem->ref_id) ? $providedValues[$bidItem->ref_id] : null,
                'weight' => $providedValues->has($bidItem->ref_id) ? null : 1,
                'propagated' => $bidItem->cost,
                'ind_giv' => $providedValues->has($bidItem->parent) ? 'Direct' : 'Bid',
                'confidence' => 1
            ];
            // if provided value...
            if($providedValues->has($bidItem->ref_id)) {
                $actual['ind_giv'] = 'Given';
                $actual['confidence'] = 1;
            }
            // get weights
            if($providedValues->hasAny(explode('.', $path)) && !$providedValues->has($bidItem->ref_id)) {
                $actual['weight'] = $bidItem->cost / $parentBid->cost;
            }
            // find all line items (leaves) and find propagated values
            if($bidItem->entity_type === 'L' && $providedValues->hasAny(explode('.', $path))) {
                $actual['propagated'] = getPropagatedValue($parentActuals, $actual['weight']);
            }
            $tmp = [
                'title' => $bidItem->title,
                'ref_id' => $bidItem->ref_id,
                'parent' => $bidItem->parent,
                'bid' => $bidItem,
                'actual' => $actual,
                'parentActual' => $parentActuals,
                'type' => $bidItem->entity_type,
                // recursively embed children
                'children' => $bidItem->children->count() ? mapToTree($bidItem, [...$parentActuals, $actual], $providedValues, $path) : collect([]),
                'path' => $path,
            ];

            return [$bidItem->ref_id => $tmp];
        });
    }



    // Map all components to tree
    $components = mapToTree($bid, [], $providedValues, null);

    // flatten array to easily add component totals together
    function flatten($componentArr) {
        $flat = [];
        foreach ($componentArr as $id => $component) {
            if(count($component['children'])) {
                $flat = [...$flat, ...flatten($component['children'])];
            }
            $flat[$id] = $component;
        }
        return collect($flat);
    }

    $flattened = flatten($components);

    // sort the component by depth in order to traverse for component prop values
    $flattened = $flattened->sortBy(function ($item) {
        return $item['bid']->depth;
    });

    foreach ($flattened as $key => $component) {
        if($component['type'] === 'C') {
            $component['actual']['propagated'] = 0;
            foreach($component['bid']->children as $child) {
                $component['actual']['propagated'] += $flattened[$child->ref_id]['actual']['propagated'];
                $flattened[$key] = $component;
            }
        }
    }

    // rebuild tree
    $components = [];
    foreach($flattened as $key => $item) {
        $components[$key] = [
            ...$item,
            'children' => $flattened->where(function($c) use ($item) {
                return $c['parent'] === $item['ref_id'];
            })
        ];
    }
    // clean up collection
    $components = collect($components)->filter(function($c) use ($id) {
        return $c['parent'] === $id;
    });


    return view('propagate')
        ->with('title', $bid->title)
        ->with('root_id', $bid->ref_id)
        ->with('components', $components);
    }
}
