<?php

use App\Http\Controllers\BidsController;
use App\Models\Bid;
use App\Models\BidComponent;
use Illuminate\Support\Facades\Route;

/**
 * Here is a link to the "Connor's proof of concept"
https://docs.google.com/spreadsheets/d/1RVKYriAzix2eGl-liYjnAtCZf_5kkrFGA5Jj5p8tEIE/edit#gid=296609145

Directions:
In that spreadsheet you will find a description of the goal and a description of the other tabs.
If you have questions,please document them so we can assess how you wrap your head into the exercise/thought process. Additionally, you'll find a simplified example of our SQL structure.

Fundamental concept:
 * Allow a user to upload a set of data (what we call Actuals) and then process that data and project it onto the original bid.  In other words, take what the actual costs of a project were and project them onto the original bid such that we can learn from the construction.
 */

Route::get('/', function () {
    return redirect('bids');
});

Route::controller(BidsController::class)->group(function () {
    Route::get('/bids', 'index');
    Route::get('/bids/{id}', 'show');
    Route::post('/bids/{id}/propagate', 'propagate');
});



//        ->map(function($item) use ($providedValues) {
//            $tmp = $item;
//            var_dump($tmp['type']);
//            if($tmp['type'] === 'L' && $providedValues->hasAny(explode('.', $tmp['path']))) {
//                $tmp['actual']['propagated'] = getPropagatedValue($tmp, $tmp['actual']['weight']);
//                var_dump($tmp['actual']['propagated']);
//                $tmp['actual']['ind_giv'] = 'Indirect';
//            }
//            return $tmp;
//        });



//    $components = $components->map(function($item, $key) use ($providedValues, $components) {
//        // get only descendants of provided valued components
//        $childIds = $providedValues->reduce(function($acc, $_, $key) use ($components) {
//            return $components[$key]['bid']->descendants;
//        })->pluck('ref_id');
//        if($item['type'] === 'L' && $childIds->contains($key)) {
//            $item['actual']['propagated'] = getPropagatedValue($key, $components, $item['actual']['weight']);
//            $item['actual']['ind_giv'] = 'Indirect';
//        }
//        return $item;
//    });

//    dd($components);
//
//    $components = $components->sortBy(function ($item) {
//        return $item['bid']->depth;
//    })->reverse();
//
//    foreach ($components as $key => $component) {
//        if($component['type'] === 'C') {
//            $component['actual']['propagated'] = 0;
//            foreach($component['bid']->children as $child) {
//                $component['actual']['propagated'] += $components[$child->ref_id]['actual']['propagated'];
//                $components[$key] = $component;
//            }
//        }
//    }
//
//
//    dd($components);


//});
