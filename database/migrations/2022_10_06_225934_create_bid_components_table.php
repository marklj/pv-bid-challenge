<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bid_components', function (Blueprint $table) {
            $table->id();
            $table->string('ref_id')->index();
            $table->string('title');
            $table->integer('cost');
            $table->integer('price');
            $table->json('config');
            $table->string('parent')->nullable();
            $table->integer('actual_cost')->nullable();
            $table->smallInteger('actual_cost_confidence_factor')->nullable();
            $table->enum('entity_type', ['C', 'L'])->default('C');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bid_components');
    }
};
