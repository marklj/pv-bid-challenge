<?php

namespace Database\Seeders;

use App\Models\BidComponent;
use App\Models\BidLineItem;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BidLineItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BidComponent::insert([
            [
                'ref_id' => 'd13pen25f',
                'title' => 'Line item 1',
                'cost' => 100,
                'price' => 125,
                'entity_type' => 'L',
                'parent' => 'ajkcs47e',
                'config' => '{"order_index": 0}'
            ],
            [
                'ref_id' => 'd13fib40k',
                'title' => 'Line item 2',
                'cost' => 200,
                'price' => 250,
                'entity_type' => 'L',
                'parent' => 'ajvfl84j',
                'config' => '{"order_index": 0}'
            ],
            [
                'ref_id' => 'd13umo65p',
                'title' => 'Line item 3',
                'cost' => 300,
                'price' => 375,
                'entity_type' => 'L',
                'parent' => 'ajvfl84j',
                'config' => '{"order_index": 1}'
            ],
            [
                'ref_id' => 'd13kqc80u',
                'title' => 'Line item 4',
                'cost' => 500,
                'price' => 625,
                'entity_type' => 'L',
                'parent' => 'ajvfl84j',
                'config' => '{"order_index": 2}'
            ],
            [
                'ref_id' => 'd13aup05a',
                'title' => 'Line item 5',
                'cost' => 800,
                'price' => 1000,
                'entity_type' => 'L',
                'parent' => 'ajgie21o',
                'config' => '{"order_index": 0}'
            ],
            [
                'ref_id' => 'd13pyd20f',
                'title' => 'Line item 6',
                'cost' => 1300,
                'price' => 1625,
                'entity_type' => 'L',
                'parent' => 'ajgie21o',
                'config' => '{"order_index": 1}'
            ],
            [
                'ref_id' => 'd13fdq45k',
                'title' => 'Line item 7',
                'cost' => 2100,
                'price' => 2625,
                'entity_type' => 'L',
                'parent' => 'ajrlx68t',
                'config' => '{"order_index": 0}'
            ],
            [
                'ref_id' => 'd13uhe60p',
                'title' => 'Line item 8',
                'cost' => 3400,
                'price' => 4250,
                'entity_type' => 'L',
                'parent' => 'ajrlx68t',
                'config' => '{"order_index": 1}'
            ],
            [
                'ref_id' => 'd13klr85u',
                'title' => 'Line item 9',
                'cost' => 2000,
                'price' => 2500,
                'entity_type' => 'L',
                'parent' => 'ajcoq05y',
                'config' => '{"order_index": 0}'
            ],
            [
                'ref_id' => 'd13apf00a',
                'title' => 'Line item 10',
                'cost' => 1000,
                'price' => 1250,
                'entity_type' => 'L',
                'parent' => 'ajnrj42d',
                'config' => '{"order_index": 0}'
            ],
        ]);
    }
}
