<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Bid
 *
 * @method static \Database\Factories\BidFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Bid newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bid newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bid query()
 */
	class Bid extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\BidComponent
 *
 * @property int $id
 * @property string $ref_id
 * @property string $title
 * @property int $cost
 * @property int $price
 * @property mixed $config
 * @property BidComponent|null $parent
 * @property int|null $actual_cost
 * @property int|null $actual_cost_confidence_factor
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Staudenmeir\LaravelAdjacencyList\Eloquent\Collection|BidComponent[] $childComponents
 * @property-read int|null $child_components_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BidLineItem[] $childLineItems
 * @property-read int|null $child_line_items_count
 * @property-read \Staudenmeir\LaravelAdjacencyList\Eloquent\Collection|BidComponent[] $children
 * @property-read int|null $children_count
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Collection|static[] all($columns = ['*'])
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent breadthFirst()
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent depthFirst()
 * @method static \Database\Factories\BidComponentFactory factory(...$parameters)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Collection|static[] get($columns = ['*'])
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent getExpressionGrammar()
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent hasChildren()
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent hasParent()
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent isLeaf()
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent isRoot()
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent newModelQuery()
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent newQuery()
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent query()
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent tree($maxDepth = null)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent treeOf(callable $constraint, $maxDepth = null)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent whereActualCost($value)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent whereActualCostConfidenceFactor($value)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent whereConfig($value)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent whereCost($value)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent whereCreatedAt($value)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent whereDepth($operator, $value = null)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent whereId($value)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent whereParent($value)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent wherePrice($value)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent whereRefId($value)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent whereTitle($value)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent whereUpdatedAt($value)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent withGlobalScopes(array $scopes)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|BidComponent withRelationshipExpression($direction, callable $constraint, $initialDepth, $from = null, $maxDepth = null)
 */
	class BidComponent extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\BidLineItem
 *
 * @property int $id
 * @property string $ref_id
 * @property string $title
 * @property int $cost
 * @property int $price
 * @property mixed $config
 * @property int|null $actual_cost
 * @property int|null $actual_cost_confidence_factor
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\BidLineItemFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|BidLineItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BidLineItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BidLineItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|BidLineItem whereActualCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BidLineItem whereActualCostConfidenceFactor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BidLineItem whereConfig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BidLineItem whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BidLineItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BidLineItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BidLineItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BidLineItem whereRefId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BidLineItem whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BidLineItem whereUpdatedAt($value)
 */
	class BidLineItem extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

