@extends('_layout')
@section('content')
    <a href=".." class="underline p-4 inline-block">&lt;- Back to Bid Detail</a>

    <h2 class="text-2xl font-bold mb-4">{{ $title }}</h2>
    <h3 class="text-lg font-semibold mb-4">Propagation Results</h3>

    <table style="width: 100%">
{{--        <tr>--}}
{{--            <td colspan="4"><strong>Bid Total</strong></td>--}}
{{--            <td><strong>${{ $totalCost }}</strong></td>--}}
{{--        </tr>--}}



        @include('partials.actualsComponent', ['items' => $components, 'depth' => 0])
    </table>

    <a href=".." class="underline p-4 inline-block">&lt;- Back to Bid Detail</a>


@endsection
