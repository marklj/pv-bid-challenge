@extends('_layout')
@section('content')

    <table>
        <tr>
            <th class="text-left">Name</th>
            <th class="text-left">Reference ID</th>
        </tr>
        @foreach($bids as $bid)
        <tr>
            <td class="pr-5"><a class="text-blue-500 underline" href="/bids/{{ $bid->ref_id }}">{{ $bid->title }}</a></td>
            <td>{{ $bid->ref_id }}</td>
        </tr>
        @endforeach
    </table>

@endsection
