@foreach($items as $item)
    <tr class="bg-green-{{400-$depth*100}}">
        <td style="padding-left: {{ $depth*20 }}px;">{{ $item['bid']['title'] }}</td>
        <td>{{ $item['actual']['provided'] }}</td>
        <td>{{ $item['actual']['weight'] }}</td>
        <td>{{ round($item['actual']['propagated'], 2) }}</td>
        <td>{{ $item['actual']['ind_giv'] }}</td>
        <td>{{-- $item['actual']['confidence'] --}}</td>
    </tr>

    @if($item['children'])
        @include('partials.actualsComponent', ['items' => $item['children'], 'depth' => $depth+1])
    @endif
@endforeach
