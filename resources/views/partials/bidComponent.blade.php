@foreach($subItems as $item)
<tr>
    <td style="padding-left: {{ $depth*20 }}px;">{{ $item->title }}</td>
    <td></td>
    <td></td>
    <td></td>
    <td>${{ $item->cost }}</td>
</tr>
@if($item->children->count())
    @include('partials.bidComponent', ['subItems' => $item->children, 'depth' => $depth + 1])
@endif
@endforeach

