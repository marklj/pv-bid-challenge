@extends('_layout')
@section('content')
    <a href=".." class="underline py-4 inline-block">&lt;- Back to Index</a>

    <h2 class="text-2xl font-bold mb-4">{{ $bid->title }}</h2>
    <table style="width: 100%">
        <tr>
            <td colspan="4"><strong>Bid Total</strong></td>
            <td><strong>${{ $totalCost }}</strong></td>
        </tr>
        @include('partials.bidComponent', ['subItems' => $items, 'depth' => 0])
    </table>

    <form method="post" action="/bids/{{ $bid->ref_id }}/propagate">
        @csrf
        <label class="block text-sm text-gray-700 mt-8 mb-1">JSON Input</label>
        <textarea class="block w-96 h-40 border border-gray-300 p-2 mb-2" name="providedValues">
[
  {
     "component_ref_id": "ajkcs47e",
     "value": 2100
  }
]
        </textarea>
        <button class="bg-blue-500 text-white px-4 py-2 rounded" type="submit">Submit</button>
    </form>

    <a href=".." class="underline p-4 inline-block">&lt;- Back to Index</a>

@endsection
